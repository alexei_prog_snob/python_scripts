#!/usr/bin/python3

import os
import time
import shutil 
import argparse
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler




class ListValidationFail(Exception):
    pass

class StrValidationFail(Exception):
    pass

class IsNotDirectory(Exception):
    pass

class IsNotExists(Exception):
    pass


class Handler(FileSystemEventHandler):
    @staticmethod
    def on_any_event(event):
        if event.is_directory:
            return None

        elif event.event_type == 'created':
            fileToMove = event.src_path
            fileName = fileToMove.split('/')[-1]
            path = fileToMove[:len(fileToMove) - len(fileName)]
            if os.path.isfile(fileToMove):
                newFileName = FixFileName(fileName)
                suffix = newFileName.split('.')[-1]
                targetPath = path + '/' + suffix
                if os.path.exists(targetPath):
                    shutil.move(fileToMove, targetPath + '/' + newFileName)

class Watcher:
    def __init__(self, _path):
        self.path = _path
        self.observer = Observer()
    
    def Run(self):
        eventHandler = Handler()
        self.observer.schedule(eventHandler, self.path, recursive = False)
        self.observer.start()
        try:
            while True:
                time.sleep(5)
        except:
            self.observer.stop()
            print('Error')
        self.observer.join()


def FixFileName(_fileName):
    '''
    @Description: remove space and add all to lowwer case
    '''
    if type(_fileName) is not str:
        raise StrValidationFail()
    return _fileName.replace(' ','_').replace('-', '_').lower()

def ValidateTheFolderPathDefineCorrectly(folderPath):
    '''
    @Description: throw exception if it is invalid input
    '''
    if type(folderPath) is not str:
       raise StrValudationFail()

    if not os.path.isdir(folderPath):
        raise IsNotDirectory()
    
    if not os.path.exists(folderPath):
        raise IsNotExists()

def ValidateAndCreateExpectedFolders(folderToOrganizePath):
    '''
    @Description: validate that all sub folders for organization are defined
    '''
    ValidateTheFolderPathDefineCorrectly(folderToOrganizePath)
    listOfFolders = ['pdf', 'epub', 'zip', 'deb']
    for subFolderName in listOfFolders:
        expectedFolder = folderToOrganizePath + '/' + subFolderName
        if not os.path.exists(expectedFolder):
            try:
                os.mkdir(expectedFolder)
            except OSError:
                print('Creation of the directory %s failed' % (expectedFolder))
            else:
                print('Successfully created the directory %s' % (expectedFolder))


def ParseArg():
    parser = argparse.ArgumentParser()
    parser.add_argument('-f' , '--folder_path', type=str, help='full path to folder to organize')
    return parser.parse_args()

if __name__ == '__main__':
    scriptData = ParseArg()
    ValidateTheFolderPathDefineCorrectly(scriptData.folder_path)
    ValidateAndCreateExpectedFolders(scriptData.folder_path)    
    watcher = Watcher(scriptData.folder_path)
    watcher.Run()
