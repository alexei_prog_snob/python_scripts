# defualt libs
import os

# pip installed libs
import openpyxl
import abc

from utils.aps_utils import StripAllNumbersFromString
from utils.aps_utils import StripAllLettersFromString
from utils.aps_utils import GetListOfLetterRange

from utils.aps_part import Capacitor
from utils.aps_part import SmallValueCapacitor


'''
@Description:
@Return: list of range all table coloms, min row and max row
'''
def GetTableRange(sheet):
    tableRange = sheet.tables.items()[0][1].split(':')
    
    letterRange = GetListOfLetterRange(StripAllNumbersFromString(tableRange[0]), 
    StripAllNumbersFromString(tableRange[1]))

    numberRange = [StripAllLettersFromString(c) for c in tableRange]
    return (letterRange, numberRange[0], numberRange[1])

class ElectroliticCapacitor(Capacitor):
    def WriteNewDataToExl(self, sheet):
        pass

class CeramicCapacitors(SmallValueCapacitor):
    def WriteNewDataToExl(self, sheet):
        # self.tableNamesList
        letterRange, firstRow, lastRow = GetTableRange(sheet)
        while True:
            written = input('Place a ceramic capaceter written value enter e for end edding: ')
            if len(written) > 3 or len(written) == 0:
                print('Error invalid value ' + value)
                continue
            
            if written == 'e':
                break

            pF = None
            if len(written) < 3: 
                pF = int(written)
            else:
                pF = int(written[:2]) * pow(10, int(written[2]))
            nF = pF // 1000.0;
            uF = nF // 1000.0;
            amount = input('please enter amount: ')
            idx = 0
            lastRow = lastRow + 1
            for letter in letterRange:
                idx = idx + 1
                dataToAdd = None
                if idx == 1:
                    dataToAdd = written
                elif idx == 2:
                    dataToAdd = str(pF)
                elif idx == 3:
                    dataToAdd = str(nF)
                elif idx == 4:
                    dataToAdd = str(uF)
                elif idx == 5:
                    dataToAdd = amount
                sheet[letter + str(lastRow)] = dataToAdd
        


if __name__ == '__main__':
    workbook = None
    fileName = './Parts.xlsx'
    if os.path.exists(fileName):
        workbook = openpyxl.load_workbook(fileName)
        print(workbook.sheetnames)
    
    ws = workbook['Ceramic Capacitors']
    print(GetTableRange(ws))
    cc = CeramicCapacitors()
    print(cc.tableNamesList)
    cc.WriteNewDataToExl(ws)
    workbook.save(fileName)
    '''
    choicesDict = dict()

    while True:
        choice = Input('Enter Choice: ')
        choicesDict[choice].WriteNewDataToExl(workbook['choice'])
    '''