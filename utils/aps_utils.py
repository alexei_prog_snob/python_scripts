

def StripAllNumbersFromString(inStr):
    return ''.join([c for c in inStr if not c.isdigit()])

def GetListOfLetterRange(begin, end):
    retList = list()
    for i in range(ord(begin), ord(end)+1):
        retList.append(chr(i))
    return retList
    
def StripAllLettersFromString(inStr):
    return int(''.join([c for c in inStr if c.isdigit()]))