import abc

class AbstractClassPart(abc.ABC):
    def __init__(self):
        super().__init__()
        self.tableNamesList = ['Written, Amount Left']

    def AddTableNamesList(self, tableName):
        self.tableNamesList.append(tableName)

    @abc.abstractmethod
    def WriteNewDataToExl(self, sheet):
        pass
    

class Capacitor(AbstractClassPart):
    def __init__(self):
        super().__init__()
        super().AddTableNamesList('uF')
    
    def WriteNewDataToExl(self, sheet):
        pass

class SmallValueCapacitor(Capacitor):
    def __init__(self):
        super().__init__()
        super().AddTableNamesList('nF')
        super().AddTableNamesList('pF')
    
    def WriteNewDataToExl(self, sheet):
        pass

class SemiConductorPart(AbstractClassPart):
    def __init__(self):
        super().__init__()
        super().AddTableNamesList('Value')
        super().AddTableNamesList('Type')

    def WriteNewDataToExl(self, sheet):
        pass